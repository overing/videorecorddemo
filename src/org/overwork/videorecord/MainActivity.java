package org.overwork.videorecord;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecordFragment fragment = new RecordFragment();
        fragment.setArguments(new Bundle());
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).commit();
    }

}
