package org.overwork.videorecord.widget;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraPreview extends SurfaceView //
        implements SurfaceHolder.Callback {

    private static final String TAG = CameraPreview.class.getSimpleName();
    private Camera mCamera;
    private boolean mSurfaceCreated;

    public CameraPreview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CameraPreview(Context context) {
        super(context);
        init(context);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");
        mSurfaceCreated = true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int f, int w, int h) {
        Log.d(TAG, "surfaceChanged");
        startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "surfaceDestroyed");
        mSurfaceCreated = false;
        stopPreview();
    }

    public void setCamera(Camera camera) {
        Log.d(TAG, "setCamera: " + camera);
        if (mCamera == camera) {
            return;
        }
        mCamera = camera;
        if (mCamera == null) {
            stopPreview();
        } else if (mSurfaceCreated) {
            startPreview();
        }
    }

    private void init(Context context) {
        Log.d(TAG, "init: " + context);
        mSurfaceCreated = false;
        getHolder().addCallback(this);
        // getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void startPreview() {
        Log.d(TAG, "startPreview");
        if (mCamera != null) {
            Camera.Parameters params = mCamera.getParameters();
            Camera.Size size = params.getSupportedPreviewSizes().get(0);
            params.setPreviewSize(size.width, size.height);
            requestLayout();
            mCamera.setParameters(params);
            try {
                mCamera.setPreviewDisplay(getHolder());
                mCamera.startPreview();
            } catch (IOException ex) {
                Log.e(TAG, "setCamera got error", ex);
            }
        }
    }

    private void stopPreview() {
        Log.d(TAG, "stopPreview");
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera = null;
        }
    }
}
