package org.overwork.videorecord;

import java.io.File;
import java.util.List;

import org.overwork.videorecord.widget.CameraPreview;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

public class RecordFragment extends Fragment //
        implements Handler.Callback, //
        CompoundButton.OnCheckedChangeListener {

    public static final String TEMP_VIDEO_FILE = "tempVideo.mp4";

    private static final String TAG = RecordFragment.class.getSimpleName();

    private static final int MSG_UPDATE_RECORD_TIME = 0x00100001;

    private int mCameraId;
    private long mRecordStartAt;

    private Handler mHandler;
    private Camera mCamera;
    private MediaRecorder mMediaRecorder;
    private File mVideoFile;

    private CameraPreview mCameraPreview;
    private CompoundButton mBtnFlash;
    private CompoundButton mBtnCamera;
    private CompoundButton mBtnCapture;
    private TextView mTextRecordTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        mHandler = new Handler(this);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
        case MSG_UPDATE_RECORD_TIME:
            long now = System.currentTimeMillis();
            long dt = now - mRecordStartAt;
            mTextRecordTime.setText(DateFormat.format("mm:ss", dt));
            mHandler.sendEmptyMessageDelayed(MSG_UPDATE_RECORD_TIME, 1000);
            return true;

        default:
            return false;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (mBtnFlash == buttonView) {
            releaseCamera();
            getCamera();
            mCameraPreview.setCamera(mCamera);
        } else

        if (mBtnCamera == buttonView) {
            mBtnFlash.setEnabled(!isChecked);
            if (mBtnFlash.isChecked()) {
                mBtnFlash.setOnCheckedChangeListener(null);
                mBtnFlash.setChecked(false);
                mBtnFlash.setOnCheckedChangeListener(this);
            }

            if (isChecked) {
                mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
            } else {
                mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
            }

            releaseCamera();
            getCamera();
            mCameraPreview.setCamera(mCamera);
        } else

        if (mBtnCapture == buttonView) {
            if (isChecked) {
                if (prepareVideoRecorder()) {
                    Log.w(TAG, "prepareVideoRecorder succeed");
                    mRecordStartAt = System.currentTimeMillis();
                    mMediaRecorder.start();
                    mHandler.sendEmptyMessage(MSG_UPDATE_RECORD_TIME);
                    mBtnFlash.setEnabled(false);
                    mBtnCamera.setEnabled(false);
                }
            } else {
                mBtnFlash.setEnabled(true);
                mBtnCamera.setEnabled(true);
                mHandler.removeMessages(MSG_UPDATE_RECORD_TIME);
                mMediaRecorder.stop();
                releaseMediaRecorder();
                AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                adb.setTitle("Video record");
                adb.setMessage(mVideoFile.toString());
                adb.setPositiveButton(android.R.string.ok, null);
                adb.setNegativeButton("View",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface d, int w) {
                                Intent view = new Intent(Intent.ACTION_VIEW);
                                view.setData(Uri.fromFile(mVideoFile));
                                startActivity(view);
                            }
                        });
                adb.show();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        int layout = R.layout.fragment_record;
        View view = inflater.inflate(layout, container, false);

        mCameraPreview = (CameraPreview) view.findViewById(R.id.preview);
        mBtnFlash = (CompoundButton) view.findViewById(R.id.btn_flash);
        mBtnCamera = (CompoundButton) view.findViewById(R.id.btn_camera_change);
        mBtnCapture = (CompoundButton) view.findViewById(R.id.btn_capture);
        mTextRecordTime = (TextView) view.findViewById(R.id.text_record_time);

        PackageManager pm = getActivity().getPackageManager();

        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            mBtnFlash.setVisibility(View.VISIBLE);
            mBtnFlash.setOnCheckedChangeListener(this);
        } else {
            mBtnFlash.setVisibility(View.GONE);
            mBtnFlash.setChecked(false);
        }

        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            mBtnCamera.setVisibility(View.VISIBLE);
            mBtnCamera.setOnCheckedChangeListener(this);
        } else {
            mBtnCamera.setVisibility(View.GONE);
            mBtnCamera.setChecked(false);
        }

        mBtnCapture.setOnCheckedChangeListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        releaseCamera();
        getCamera();
        mCameraPreview.setCamera(mCamera);
    }

    @Override
    public void onPause() {
        mCameraPreview.setCamera(null);
        releaseMediaRecorder();
        releaseCamera();
        super.onPause();
    }

    private boolean prepareVideoRecorder() {
        releaseCamera();
        getCamera();
        mCamera.unlock();
        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setCamera(mCamera);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        int QUALITY_480P = CamcorderProfile.QUALITY_480P;
        int QUALITY_HIGH = CamcorderProfile.QUALITY_HIGH;
        CamcorderProfile profile;
        if (mBtnCamera.isChecked() && CamcorderProfile.hasProfile(QUALITY_480P)) {
            profile = CamcorderProfile.get(mCameraId, QUALITY_480P);
        } else {
            profile = CamcorderProfile.get(mCameraId, QUALITY_HIGH);
        }
        mMediaRecorder.setProfile(profile);

        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mMediaRecorder.setOrientationHint(270);
        } else {
            mMediaRecorder.setOrientationHint(90);
        }

        // limit video length
        // mMediaRecorder.setMaxDuration(8000);

        File cacheDir;
        String storageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(storageState)) {
            cacheDir = getActivity().getExternalCacheDir();
        } else {
            cacheDir = getActivity().getCacheDir();
        }
        mVideoFile = new File(cacheDir, TEMP_VIDEO_FILE);
        if (mVideoFile.exists()) {
            mVideoFile.delete();
        }
        mMediaRecorder.setOutputFile(mVideoFile.getAbsolutePath());

        Surface surface = mCameraPreview.getHolder().getSurface();
        mMediaRecorder.setPreviewDisplay(surface);

        try {
            mMediaRecorder.prepare();
        } catch (Exception e) {
            Log.e(TAG, "Preparing MediaRecorder got error", e);
            releaseMediaRecorder();
            // TODO show for user ?
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
    }

    private void getCamera() {
        try {
            mCamera = Camera.open(mCameraId);

            // configuration camera

            mCamera.setDisplayOrientation(90);

            Camera.Parameters params = mCamera.getParameters();

            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            Camera.Size size = sizes.get(0);
            params.setPictureSize(size.width, size.height);

            String FOCUS_MODE_CONTINUOUS_VIDEO = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO;
            String FOCUS_MODE_INFINITY = Camera.Parameters.FOCUS_MODE_INFINITY;
            if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                List<String> modes = params.getSupportedFocusModes();
                if (modes.contains(FOCUS_MODE_CONTINUOUS_VIDEO)) {
                    params.setFocusMode(FOCUS_MODE_CONTINUOUS_VIDEO);
                } else if (modes.contains(FOCUS_MODE_INFINITY)) {
                    params.setFocusMode(FOCUS_MODE_INFINITY);
                }
            }

            if (mBtnFlash.isChecked()) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            } else {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            }

            mCamera.setParameters(params);
        } catch (Exception ex) {
            Log.e(TAG, "getCameraInstance got error", ex);
            // Camera is not available (in use or does not exist)
            // TODO show for user ?
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }
}
